﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;

namespace PTS.Tests.Fakes
{
   /// <summary>
   /// This is a copy of the original but without inheritance of the GTA
   /// Script class. So it lacks of all functionality referencing that.
   /// </summary>
   internal class LocationHandling
   {
      private static readonly object SyncLock = new object();
      private readonly JobElements _jobElements;

      public LocationHandling(JobElements jobElements)
      {
         _jobElements = jobElements;
         Locations = new List <Location>();
      }

      public LocationHandling()
      {
         Locations = new List <Location>();
      }

      internal bool IgnoreOpenDays { private get; set; }
      internal bool IgnoreOpenHours { private get; set; }

      /// <summary>
      /// Gets the locations.
      /// </summary>
      /// <value>
      /// The locations.
      /// </value>
      internal List <Location> Locations { get; private set; }

      internal static string CreateCustomerText(Location pickupLocation)
      {
         return String.Format("Pick up a ~b~{0}~w~ at ~y~{1}~w~.",
            pickupLocation.PersonType, pickupLocation.TargetName);
      }

      internal static string CreateDestinationText(Location destination, string customerType)
      {
         return String.Format("Drive the {0} to ~p~{1}~w~.", customerType, destination.TargetName);
      }

      internal static bool IsWithinOpeningHours(byte availableFrom, byte availableUntil, TimeSpan currentTime)
      {
         var openFrom = new TimeSpan(availableFrom, 0, 0);
         var openUntil = new TimeSpan(availableUntil, 0, 0);
         if(openUntil <= openFrom)
            openUntil += new TimeSpan(24, 0, 0);
         if(currentTime < openFrom)
            currentTime += new TimeSpan(24, 0, 0);
         return currentTime >= openFrom && currentTime <= openUntil;
      }

      internal static List <Location> TruncateLongLocationNames(List <Location> locations, int maxLength)
      {
         foreach(var location in locations.Where(location => location.TargetName.Length > maxLength))
         {
            location.TargetName = location.TargetName.Substring(0, maxLength - 3) + "...";
         }
         return locations;
      }
      
      /// <summary>
      /// Resets all Location.Visited flags to false and saves them to
      ///  PTSLocations.xml.
      /// </summary>
      internal void ResetVisited()
      {
         if(Locations.Count == 0)
            ReadFullLocationList(CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
         foreach(var location in Locations)
         {
            location.Visited = false;
         }
         SaveLocationList(Locations, CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
      }

      private DistanceIdentifier GetNearestDistanceIdentifier(ref List<DistanceIdentifier> distanceIdentifiers)
      {
         var tempIdentifier = new DistanceIdentifier { Distance = float.MaxValue };
         foreach(var identifier in distanceIdentifiers)
         {
            if(identifier.Distance < tempIdentifier.Distance)
               tempIdentifier = identifier;
         }
         return tempIdentifier;
      }

      private bool IsWithinOpenDays(IEnumerable<DayOfWeek> openDays, DayOfWeek currentDay)
      {
         return openDays.Any(d => d == currentDay);
      }
      
      private void ReadFullLocationList(string fullPath)
      {
         Locations = new XmlIo(fullPath)
            .Load<List<Location>>();
      }

      /// <summary>
      /// Resets the visited tag of all locations if less than 2 locations are not visited.
      /// </summary>
      /// <note>Two locations (pickup and destination) are needed with each new job.</note>
      private void ResetVisitedIfLessThan2()
      {
         int counter = 0;
         foreach(var location in Locations)
         {
            if(!location.Visited)
               counter++;
            if(counter >= 2)
               return;
         }
         ResetVisited();
      }

      private void SaveLocationList(List<Location> targetList, string fullPath)
      {
         new XmlIo(fullPath).Save(targetList);
      }

      private List<Location> TakeStreetNameIfLocationNameIsNotSet(List<Location> locations)
      {
         foreach(var location in locations)
         {
            if(location.TargetName == "placeholder"
               || location.TargetName == "."
               || string.IsNullOrEmpty(location.TargetName))
            {
               location.TargetName = location.StreetName;
            }
         }
         return locations;
      }
   }
}