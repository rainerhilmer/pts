﻿using System;
using System.Collections.Generic;
using Cyron43.GtaIV.Common;
using NUnit.Framework;

namespace PTS.Tests
{
   /// <summary>
   /// This special class is used to update the current locations list with new properties.
   /// </summary>
   [TestFixture]
   public class LocationsUpdater
   {
      private readonly string _fullPath = Environment.GetFolderPath(
         Environment.SpecialFolder.MyDocuments)
                                          + @"\GTA IV\ModSettings\PTSLocations.xml";

      [Test]
      public void Update()
      {
         var locations = new XmlIo(_fullPath).Load<List<Location>>();
         foreach (var location in locations)
         {
            location.Visited = false;
         }
         new XmlIo(_fullPath).Save(locations);
      }
   }
}
