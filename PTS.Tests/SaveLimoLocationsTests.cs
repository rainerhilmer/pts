﻿using System.Collections.Generic;
using System.IO;
using Cyron43.GtaIV.Common;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace PTS.Tests
{
   [TestFixture]
   public class SaveLimoLocationsTests
   {
      private const string FILE = @"a:\TargetsTest.xml";
      private readonly XmlIo _fileHandler = new XmlIo(FILE);
      private List<Location> _targets = new List<Location>();

      [Test]
      public void Creates_file_with_target_list()
      {
         var fixture = new Fixture();
         var target = new Location
                         {
                            X = fixture.Create<float>(),
                            Y = fixture.Create<float>(),
                            Z = fixture.Create<float>(),
                            StreetName = fixture.Create<string>(),
                            TargetName = fixture.Create<string>(),
                            PersonType = fixture.Create<string>()
                         };
         SaveLocation(target);
      }

      private void SaveLocation(Location location)
      {
         if (!File.Exists(FILE))
         {
            _targets.Add(location);
            _fileHandler.Save(_targets);
            return;
         }
         _targets.Clear();
         _targets = _fileHandler.Load<List<Location>>();
         _targets.Add(location);
         _fileHandler.Save(_targets);
      }
   }
}