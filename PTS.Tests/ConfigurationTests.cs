﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using NUnit.Framework;
using FluentAssertions;
using Cyron43.GtaIV.Common.FakesForTests;
using KeyEventArgs = Cyron43.GtaIV.Common.FakesForTests.KeyEventArgs;

namespace PTS.Tests
{
   [TestFixture]
   public class ConfigurationTests
   {
      private ConfigurationContainer _config;
      private ErrorType _typeOfError;

      [TestFixtureSetUp]
      public void Setup()
      {
         _config = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(
               ModIdentityProvider.Identity,
               out _typeOfError,
               supressInGameMessage: true);
      }

      [Test]
      public void CompareKeys_with_alt_and_l_returns_true()
      {
         var keyContainer = new KeyContainer
                            {
                               Alt = true,
                               Key = Keys.L
                            };
         var keyEventArgs = new KeyEventArgs
                            {
                               Alt = true,
                               Key = Keys.L
                            };
         CompareKeys(keyContainer, keyEventArgs).Should().BeTrue();
      }

      [Test]
      public void Reads_config()
      {
         _config.Should().BeOfType<ConfigurationContainer>().And.Should().NotBeNull();
         Console.WriteLine("Ignore open hours: {0}, vehicle name: {1}", _config.IgnoreOpenHours, _config.VehicleName);
         _typeOfError.Should().Be(ErrorType.None);
      }

      [Test]
      public void Reads_passenger_seat()
      {
         VehicleSeat passengerSeat;
         Enum.TryParse(_config.PassengerSeat, out passengerSeat).Should().BeTrue();
         Console.WriteLine(passengerSeat);
      }

      [Test]
      public void ReadsParticularKeyContainer()
      {
         _config.Should().NotBeNull("_config ist null.");
         _config.Keys.Should().NotBeNull("_config.Keys ist null");
         var keyContainer = _config.Keys.Find(key => key.Name == "ResetVisited");
         keyContainer.Should().NotBeNull("keyContainer is null");
         keyContainer.Alt.Should().BeTrue();
         keyContainer.Ctrl.Should().BeTrue();
         keyContainer.Key.Should().Be(Keys.R);
      }

      [Test, Explicit]
      public void Saves_the_config()
      {
         var keys = new List<KeyContainer>
                    {
                       new KeyContainer
                       {
                          Alt = true,
                          Ctrl = false,
                          Key = Keys.L,
                          Name = "RequestStartupOrShutdown",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = true,
                          Ctrl = false,
                          Key = Keys.S,
                          Name = "ImmediateShutdown",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.L,
                          Name = "LocationMap",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.S,
                          Name = "SaveNewLocation",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.R,
                          Name = "Failover",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.D,
                          Name = "HQLessMode",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = true,
                          Ctrl = true,
                          Key = Keys.R,
                          Name = "ResetVisited",
                          Shift = false
                       }
                    };
         var config = new ConfigurationContainer
                      {
                         CustomerModel = string.Empty,
                         DisplayTimeOfNextJob = false,
                         EnableSaveNewLocation = true,
                         FineOnDeath = -500,
                         FineOnInjury = -100,
                         HQLessByDefault = true,
                         IgnoreOpenDays = false,
                         IgnoreOpenHours = false,
                         Income = 100,
                         JobDelayMax = 2,
                         JobDelayMin = 0,
                         Keys = keys,
                         PassengerSeat = "RightFront",
                         SpawnBodyguard = false,
                         TakeNearestLocationForNextJob = true,
                         VehicleName = "stretch",
                         Version = ModIdentityProvider.Identity.Version
                      };
         ConfigurationProvider.SaveConfig(config, @"a:\PTSConfig.xml");
      }

      // Nicht durch KeyHandlingCommons.KeyIs ersetzen, weil hier fake KeyEventArgs benutzt wird.
      private static bool CompareKeys(KeyContainer keyContainer, KeyEventArgs keyEventArgs)
      {
         return keyContainer.Alt == keyEventArgs.Alt
                && keyContainer.Ctrl == keyEventArgs.Control
                && keyContainer.Shift == keyEventArgs.Shift
                && keyContainer.Key == keyEventArgs.Key;
      }
   }
}