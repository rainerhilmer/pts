﻿using System;
using System.IO;

namespace PTS.Tests
{
   /// <summary>
   /// Kapselt eine Methode, die einen Stream verwaltet.
   /// </summary>
   [Obsolete("Reserved for future use.", false)]
   public delegate void StreamHandle();

   /// <summary>
   /// Repräsentiert ein Instrumentarium für das Schreiben und Lesen
   ///  von Logfiles in verschiedenen Formaten.
   /// Diese Klasse ist abstrakt.
   /// </summary>
   public abstract class Logger : IDisposable
   {
      #region Public Properties

      /// <summary>
      /// Ermittelt ob das angegebene Logfile bereits existiert.
      /// </summary>
      /// <value><c>true</c> falls es existiert; anderenfalls <c>false</c>.</value>
      public bool Exists
      {
         get
         {
            var info = new FileInfo(FullLogName);
            return info.Exists;
         }
      }

      /// <summary>
      /// Ruft den voll qualifizierten Lognamen ab.
      /// </summary>
      public string FullLogName { get; protected set; }

      #endregion Public Properties

      #region Public Methods

      /// <summary>
      /// Performs application-defined tasks associated with freeing,
      /// releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      /// <summary>
      /// Erstellt ein neues Logfile.
      /// Sollte bereits ein Logfile mit gleichem Namen am gleichen Ort vorhanden sein,
      /// wird es überschrieben.
      /// </summary>
      /// <param name="title">Die Titelzeile im Logfile</param>
      public abstract void Create(string title);

      /// <summary>
      /// Löscht das im Konstruktor der Klasse angegebene Logfile.
      /// </summary>
      public abstract void Delete();

      /// <summary>
      /// Gibt Resourcen frei.
      /// </summary>
      public virtual void Dispose(bool disposing)
      {
         if (disposing)
         {
            // Freigeben von managed objects.
         }

         // Freigeben von unmanaged objects.
      }

      /// <summary>
      /// Holt den Pfad zum aktuellen Projekt-Ordner.
      /// Dieses Property bitte nicht im Produktivcode benutzen.
      /// </summary>
      /// <returns>der Pfad zum aktuellen Projekt-Ordner.</returns>
      public static string GetProjectPath(EnvironmentMode mode)
      {
         // Ergebnis: Debug- oder Release-Ordner im Projektordner.
         string projectPath = Environment.CurrentDirectory;

         // Mit jedem Durchlauf geht es im Verzeichnisbaum eine Stufe höher.
         for (int i = 0; i < (int) mode; i++)
         {
            projectPath = Path.GetDirectoryName(projectPath);
         }
         return projectPath + @"\";
      }

      /// <summary>
      /// Liest das im Konstruktor der Klasse angegebene Logfile komplett in den Speicher.
      /// </summary>
      /// <returns>Das Logfile</returns>
      public abstract string Read();

      /// <summary>
      /// Schreibt einen neuen Eintrag in ein mit Create erstelltes Logfile.
      /// </summary>
      /// <param name="entry">Eine Logzeile</param>
      /// <param name="actualDateAndTime">Das aktuelle Datum und die Uhrzeit</param>
      public abstract void Write(string entry, DateTime actualDateAndTime);

      /// <summary>
      /// Schreibt einen neuen Eintrag in ein mit Create erstelltes Logfile.
      /// </summary>
      /// <param name="entry">Eine Logzeile</param>
      public abstract void Write(string entry);

      #endregion Public Methods

      #region Destructors

      /// <summary>
      /// Gibt Resourcen frei.
      /// </summary>
      ~Logger()
      {
         // Simply call Dispose(false).
         Dispose(false);
      }

      #endregion Destructors

      #region Enumerations

      /// <summary>
      /// Je nach Umgebung variiert die Anzahl der Stufen,
      /// die in der Ordnerstruktur nach oben geklettert werden muß.
      /// </summary>
      public enum EnvironmentMode
      {
         /// <summary>
         /// Klettert in der Ordnerstruktur 2 Stufen nach oben.
         /// </summary>
         Regular = 2,

         /// <summary>
         /// Klettert in der Ordnerstruktur 3 Stufen nach oben.
         /// </summary>
         UnitTests
      }

      #endregion Enumerations
   }
}