﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using NUnit.Framework;
using FluentAssertions;

namespace PTS.Tests
{
   [TestFixture]
   public class LocationHandlingTests
   {
      private readonly string _fullPath = CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml";

      [Test]
      public void CurrentlyAvailableLocations_is_smaller_than_all_locations()
      {
         var sut = new LocationHandling();
         sut.GetPreparedLocationList(_fullPath);
         var allLocations = sut.Locations;
         var currentTime = new TimeSpan(9, 2, 0);
         sut.CreateListOfCurrentlyAvailableLocations(
            currentTime, DayOfWeek.Friday, _fullPath);
         sut.Locations.Count.Should().BeLessThan(allLocations.Count);
         foreach(var location in sut.Locations)
         {
            Console.WriteLine("Location name: {0}, open from: {1}, open until: {2}",
                              location.TargetName, location.AvailableFrom, location.AvailableUntil);
         }
      }

      [Test]
      public void Gets_closest_location()
      {
         var sut = new LocationHandling();
         sut.GetPreparedLocationList(_fullPath);
         var nextToNearestLocation = sut.GetNextToNearestLocation();
         Console.WriteLine("X:{0}/Y:{1}/Z:{2}",
            nextToNearestLocation.X, nextToNearestLocation.Y, nextToNearestLocation.Z);
      }

      [Test]
      public void IsWithinOpeningHours_from_22_to_4_with_2_returns_true()
      {
         byte availableFrom = 22;
         byte availableUntil = 4;
         var currentTime = new TimeSpan(2, 0, 0);
         LocationHandling.IsWithinOpeningHours(availableFrom, availableUntil, currentTime).Should().BeTrue();
      }

      [Test]
      public void IsWithinOpeningHours_from_22_to_4_with_21_returns_false()
      {
         byte availableFrom = 22;
         byte availableUntil = 4;
         var currentTime = new TimeSpan(21, 0, 0);
         LocationHandling.IsWithinOpeningHours(availableFrom, availableUntil, currentTime).Should().BeFalse();
      }

      [Test]
      public void IsWithinOpeningHours_from_22_to_4_with_23_returns_true()
      {
         byte availableFrom = 22;
         byte availableUntil = 4;
         var currentTime = new TimeSpan(23, 0, 0);
         LocationHandling.IsWithinOpeningHours(availableFrom, availableUntil, currentTime).Should().BeTrue();
      }

      [Test]
      public void IsWithinOpeningHours_from_9_to_17_with_11_returns_true()
      {
         byte availableFrom = 9;
         byte availableUntil = 17;
         var currentTime = new TimeSpan(11, 0, 0);
         LocationHandling.IsWithinOpeningHours(availableFrom, availableUntil, currentTime).Should().BeTrue();
      }

      [Test]
      public void IsWithinOpeningHours_from_9_to_17_with_8_returns_false()
      {
         byte availableFrom = 9;
         byte availableUntil = 17;
         var currentTime = new TimeSpan(8, 0, 0);
         LocationHandling.IsWithinOpeningHours(availableFrom, availableUntil, currentTime).Should().BeFalse();
      }

      /// <summary>
      /// "Das ist kein Test, sondern für die manuelle Verifizierung des XML-Files gedacht."
      /// </summary>
      [Test]
      [Ignore]
      public void Log_targets()
      {
         var sut = new LocationHandling();
         sut.GetPreparedLocationList(_fullPath);
         var log = new TextLogger(@"A:\PTS\Targets.txt");
         log.Create(string.Format("Amount of targets: {0}", sut.Locations.Count));
         foreach(var location in sut.Locations)
         {
            log.Write(location.TargetName);
         }
      }

      [Test]
      public void ReadLocationList_location_names_are_not_longer_than_20()
      {
         var sut = new LocationHandling();
         sut.GetPreparedLocationList(Environment.CurrentDirectory + @"\PTSLocations.xml");
         foreach(var location in sut.Locations)
         {
            Console.WriteLine(location.TargetName);
            location.TargetName.Length.Should().BeLessOrEqualTo(30);
         }
      }

      [Test]
      public void Reads_locations()
      {
         new XmlIo(CommonFunctions.FileRepositoryPath + "PTSLocations.xml").Load<List<Location>>();
      }

      [Test]
      public void Reads_target_list()
      {
         var sut = new LocationHandling();
         sut.GetPreparedLocationList(_fullPath);
         sut.Locations.Count.Should().BeGreaterThan(0);
      }

      [Test]
      public void ResetVisited_does_not_destroy_locations_list()
      {
         var locationsBefore = new XmlIo(Environment.CurrentDirectory + @"\PTSLocations.xml")
            .Load<List<Location>>();
         foreach(var location in locationsBefore)
         {
            location.Visited = true;
         }
         var sut = new Fakes.LocationHandling();
         sut.ResetVisited();
         var locationsAfter = new XmlIo(Environment.CurrentDirectory + @"\PTSLocations.xml")
            .Load<List<Location>>();
         locationsAfter.Count.ShouldBeEquivalentTo(locationsBefore.Count, "amount of locations not equal!");
         var counter = locationsAfter.Count(location => location.Visited);
         counter.Should().Be(0, "all locations should be set to not vsited.");
      }

      /// <summary>
      /// "Das ist kein Test, sondern für Rewrites des XML-Files gedacht."
      /// </summary>
      [Test]
      [Ignore]
      public void Rewrite_target_list()
      {
         // Example:
         //var targetHandler = new LocationHandling();
         //var targetList = targetHandler.GetPreparedLocationList(_fullPath);
         //for (int i = 0; i < targetList.Count; i++)
         //{
         //   targetList[i].Id = i + 1;
         //}
         //targetHandler.SaveLocationList(targetList, _fullPath);
      }

      [Test]
      [Ignore]
      public void Spike()
      {
         var current = DateTime.Now.TimeOfDay;
         var newTime = new TimeSpan(current.Hours, current.Minutes + 61, current.Seconds);
         Console.WriteLine("Current time: " + current);
         Console.WriteLine("New time: " + newTime);
      }

      [Test]
      public void TruncateLongLocationNames_truncates_as_expected()
      {
         const int maxLength = 20;
         var locations = new XmlIo(Environment.CurrentDirectory + @"\PTSLocations.xml")
            .Load<List<Location>>();
         var modifiedLocations = LocationHandling.TruncateLongLocationNames(locations, maxLength);
         foreach (var location in modifiedLocations)
         {
            Console.WriteLine(location.TargetName);
            location.TargetName.Length.Should().BeLessOrEqualTo(maxLength);
         }
      }
   }
}