﻿using System;
using System.Collections.Generic;
using Cyron43.GtaIV.Common;
using NUnit.Framework;
using FluentAssertions;

namespace PTS.Tests
{
   [TestFixture]
   public class CommonFunctionsTests
   {
      [Test]
      public void GetRandomIntegerNumber_returns_numbers_between_0_and_1_in_this_case()
      {
         var numberList = new List<int>();
         for(int i = 0; i < 10; i++)
         {
            var randomNumber = CommonFunctions.GetRandomIntegerNumber(0, 1);
            numberList.Add(randomNumber);
            Console.WriteLine(randomNumber);
         }
         numberList.Should().Contain(0, "no 0 in the list.");
         numberList.Should().Contain(1, "no 1 in the list");
      }

      [Test]
      public void GetRandomIntegerNumber_does_not_generate_numbers_greater_than_max()
      {
         for(int i = 0; i < 1000; i++)
         {
            var number=CommonFunctions.GetRandomIntegerNumber(0, 10);
            number.Should().BeLessOrEqualTo(10, "Number is " + number);
         }
      }
   }
}