﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Cyron43.GtaIV.Common;

namespace PTS
{
   [KnownType(typeof(DayOfWeek))]
   [KnownType(typeof(TransportationMode))]
   public class Location : ILocation
   {
      public byte AvailableFrom { get; set; }
      public byte AvailableUntil { get; set; }
      public int Id { get; set; }
      public List<DayOfWeek> OpenDays { get; set; }
      public byte PersonPriority { get; set; }

      /// <summary>
      ///    z.B. "foreign politician", "commedy star". Falls leer, soll automatisch "someone" erscheinen.
      /// </summary>
      public string PersonType { get; set; }

      /// <summary>
      ///    Wird von GTA geliefert.
      /// </summary>
      public string StreetName { get; set; }

      /// <summary>
      ///    z.B. "Uno building", "Hotel Majestic", ...
      /// </summary>
      public string TargetName { get; set; }

      public TransportationMode TransportationMode { get; set; }
      public bool Visited { get; set; }
      public float X { get; set; }
      public float Y { get; set; }
      public float Z { get; set; }
   }
}