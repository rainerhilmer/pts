using System;
using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   // ReSharper disable once ClassNeverInstantiated.Global
   public class Core : Script
   {
      private static readonly object SyncLock = new object();
      private readonly KeyHandling _keyHandling;
      private readonly StateBroker _stateBroker;
      internal bool HQLessModeManuallySwitched;
      internal ErrorType TypeOfError;

      public Core()
      {
         JobElements = new JobElements(new Checkpoint())
                       {
                          ServiceLocationsFullFilePath =
                             CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml"
                       };
         EstablishConfigFromFile();
         if(TypeOfError != ErrorType.None)
            return;
         _keyHandling = new KeyHandling(this);
         Status = new StateObjects();
         _stateBroker = new StateBroker(JobElements)
                        {
                           FineOnDeath = Configuration.FineOnDeath,
                           FineOnInjury = Configuration.FineOnInjury,
                           Income = Configuration.Income
                        };
         _stateBroker.RegisterJobs();
         KeyDown += OnKeyDown;
         Interval = 600;
      }

      internal ConfigurationContainer Configuration { get; private set; }
      internal JobElements JobElements { get; private set; }
      internal StateObjects Status { get; private set; }

      internal void OnTick(object sender, EventArgs e)
      {
         lock(SyncLock)
         {
            if(!JobElements.ModIsActive)
               return;
            _stateBroker.RunJob(JobElements.NextState);
         }
      }

      internal void RequestShutdown()
      {
         if(!JobElements.IsOnDuty)
         {
            JobElements.ImmediateShutDown();
            Tick -= OnTick;
         }
         if(JobElements.NextState == ServiceState.HeadingForDestination
            || JobElements.NextState == ServiceState.ShutDownRequested)
         {
            CommonFunctions.DisplayText("You must deliver the customer before you can end the service.", 5000);
            JobElements.NextState = ServiceState.ShutDownRequested;
            JobElements.ShutdownRequested = true;
            return;
         }
         JobElements.NextState = ServiceState.ShutdownApproved;
      }

      internal void Trigger()
      {
         // Ja hier auch, weil die Config sonst nur mit der Aktivierung des Scripts gelesen wird.
         EstablishConfigFromFile();
         if(TypeOfError != ErrorType.None)
         {
            JobElements.ImmediateShutDown();
            KeyDown -= OnKeyDown;
            Tick -= OnTick;
            return;
         }
         if(JobElements.HQLess && Player.Character.isInVehicle())
         {
            JobElements.CurrentVehicle = Player.Character.CurrentVehicle;
            JobElements.NextState = ServiceState.Start;
            Tick += OnTick;
            JobElements.ModIsActive = true;
            CommonFunctions.DisplayText("Transport service is now activated.", 5000);
         }
         if(JobElements.HQLess && !Player.Character.isInVehicle())
         {
            CommonFunctions.DisplayText("Enter a vehicle and try again.", 3000);
         }
         else if(!JobElements.HQLess)
         {
            JobElements.NextState = ServiceState.Start;
            Tick += OnTick;
            JobElements.ModIsActive = true;
            JobElements.HQElementsEstablished = true;
            JobElements.TransportHq = new Vector3(79.2246246f, -706.903931f, 5.001522f);
            JobElements.VehicleSpawnPosition = new Vector3(90.5848f, -714.4081f, 4.582633f).ToGround();
            JobElements.CreateVehicleAtHQ(firstTime: true);
            JobElements.HQBlip = Blip.AddBlip(new Vector3(79.2246246f, -706.903931f, 5.001522f));
            JobElements.HQBlip.Icon = BlipIcon.Person_Lawyer;
            JobElements.HQBlip.Name = "PTS HQ";
            CommonFunctions.DisplayText("Transport service is now activated."
                                        + " Go to the transport service HQ to go on duty.", 5000);
         }
      }

      private void EstablishConfigFromFile()
      {
         Configuration = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out TypeOfError);
         if(TypeOfError != ErrorType.None)
            return;
         if(!HQLessModeManuallySwitched)
            JobElements.HQLess = Configuration.HQLessByDefault;
         JobElements.VehicleModelName = Configuration.VehicleName;
         JobElements.CustomerModel = Configuration.CustomerModel;
         JobElements.LocationHandling.IgnoreOpenDays = Configuration.IgnoreOpenDays;
         JobElements.LocationHandling.IgnoreOpenHours = Configuration.IgnoreOpenHours;
         JobElements.JobHandler.DisplayTimeOfNextJob = Configuration.DisplayTimeOfNextJob;
         JobElements.JobHandler.JobDelayMin = Configuration.JobDelayMin;
         JobElements.JobHandler.JobDelayMax = Configuration.JobDelayMax;
         JobElements.SpawnBodyguard = Configuration.SpawnBodyguard;
         JobElements.TakeNearestLocationForNextJob = Configuration.TakeNearestLocationForNextJob;
         VehicleSeat passengerSeat;
         if(!Enum.TryParse(Configuration.PassengerSeat, out passengerSeat))
            throw new ArgumentException("Passenger seat not found.");
         JobElements.PassengerSeat = passengerSeat;
      }

      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         _keyHandling.HandleKey(e);
      }
   }
}