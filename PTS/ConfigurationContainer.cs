﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common;

namespace PTS
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public string CustomerModel { get; set; }
      public bool DisplayTimeOfNextJob { get; set; }
      public bool EnableSaveNewLocation { get; set; }
      public int FineOnDeath { get; set; }
      public int FineOnInjury { get; set; }
      public bool HQLessByDefault { get; set; }
      public bool IgnoreOpenDays { get; set; }
      public bool IgnoreOpenHours { get; set; }
      public int Income { get; set; }

      /// <summary>
      ///    Maximum delay between two jobs in game minutes.
      /// </summary>
      public uint JobDelayMax { get; set; }

      /// <summary>
      ///    Minimum delay between two jobs in game minutes.
      /// </summary>
      public uint JobDelayMin { get; set; }

      /// <summary>
      ///    Enthält die Keyboard-Steuercodes für den Mod.
      /// </summary>
      public List<KeyContainer> Keys { get; set; }

      /// <summary>
      ///    Bei 4-Sitzern "RightRear" und bei 2-Sitzern "RightFront".
      /// </summary>
      public string PassengerSeat { get; set; }

      public bool SpawnBodyguard { get; set; }
      public bool TakeNearestLocationForNextJob { get; set; }

      /// <summary>
      ///    So wie der Name in der Vehicles.ide steht.
      /// </summary>
      public string VehicleName { get; set; }

      public int Version { get; set; }
   }
}