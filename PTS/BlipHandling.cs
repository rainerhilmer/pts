﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   internal static class BlipHandling
   {
      private static readonly List<Blip> Blips = new List<Blip>();
      private static readonly object SyncLock = new object();
      // ReSharper disable once InconsistentNaming
      private static LocationHandling _locationHandling;
      private static bool blipMapActive;

      internal static Blip SetCustomerBlip(Vector3 pickupVector)
      {
         lock(SyncLock)
         {
            var customerBlip = Blip.AddBlip(pickupVector);
            customerBlip.Friendly = true;
            customerBlip.Name = "Customer";
            customerBlip.RouteActive = true;
            return customerBlip;
         }
      }

      internal static Blip SetDestinationBlip(Vector3 destinationVector, Location destination)
      {
         lock(SyncLock)
         {
            var destinationBlip = Blip.AddBlip(destinationVector.ToGround());
            destinationBlip.Icon = BlipIcon.Misc_Destination;
            destinationBlip.Name = destination.TargetName;
            destinationBlip.RouteActive = true;
            destinationBlip.Display = BlipDisplay.ArrowAndMap;
            destinationBlip.Color = BlipColor.Purple;
            return destinationBlip;
         }
      }

      internal static Blip SpawnLimoBlip(Vehicle limo)
      {
         lock(SyncLock)
         {
            var limoBlip = limo.AttachBlip();
            limoBlip.Name = "Your Vehicle";
            limoBlip.Friendly = true;
            limo.DoorLock = DoorLock.None;
            return limoBlip;
         }
      }

      internal static void TriggerLocationMap(
         LocationHandling locationHandling, string fullPath, Vector3 currentPosition)
      {
         lock(SyncLock)
         {
            _locationHandling = locationHandling;
            var locations = new List<Location>();
            if(!blipMapActive)
            {
               locationHandling.GetPreparedLocationList(fullPath);
               locations = locationHandling.Locations;
               CreateBlipsFromLocations(ref locations, currentPosition);
               blipMapActive = true;
            }
            else
            {
               foreach(var blip in Blips)
               {
                  blip.Display = BlipDisplay.Hidden;
                  blip.Delete();
               }
               Blips.Clear();
               locations.Clear();
               GC.Collect();
               blipMapActive = false;
            }
         }
      }

      private static void CreateBlipsFromLocations(ref List<Location> locations, Vector3 currentPosition)
      {
         lock(SyncLock)
         {
            var nearbyLocations = GetLocationsForDisplay(ref locations, currentPosition);
            if(nearbyLocations == null)
               return;
            var enumerable = nearbyLocations as IList<Location> ?? nearbyLocations.ToList();
            if(!enumerable.Any())
               return;
            foreach(var location in enumerable)
            {
               if(location == null)
                  continue;
               var blip = Blip.AddBlip(new Vector3(location.X, location.Y, location.Z));
               if(blip == null)
                  continue;
               blip.Icon = BlipIcon.Misc_Destination;
               blip.RouteActive = false;
               blip.Display = BlipDisplay.MapOnly;
               blip.Color = SetBlipColor(location);
               blip.Name = "ID" + location.Id.ToString(CultureInfo.InvariantCulture);
               Blips.Add(blip);
            }
         }
      }

      private static List<Location> Get30ClosestLocations(ref List<Location> locations, ref Vector3 currentPosition)
      {
         if(locations.Count < 31)
            return locations;
         locations.Sort(new LocationComparer(currentPosition).Compare);
         var rangeEnd = locations.Count - 30;
         locations.RemoveRange(30, rangeEnd);
         return locations;
      }

      //note: the locations injected in this method are temporary already.
      private static IEnumerable<Location> GetLocationsForDisplay(ref List<Location> locations, Vector3 currentPosition)
      {
         /* Not .All(...) because if I miss to change just one PersonType to
          * delivery the map will display the closest locations only. */
         if(locations.Any(location => location.PersonType.ToLower() == "delivery"))
            return locations;
         if(!_locationHandling.GetCurrentTransportationMode().Equals(TransportationMode.Ground))
            return locations;
         var tempLocations = new List<Location>(locations);
         locations = Get30ClosestLocations(ref locations, ref currentPosition);
         bool includesID1 = locations.Any(location => location.Id == 1);
         if(!includesID1)
            locations.Add(tempLocations.Find(x => x.Id == 1));
         return locations;
      }
      
      private static BlipColor SetBlipColor(Location location)
      {
         if(location.Id == 1)
            return BlipColor.Yellow;

         if(location.TransportationMode == TransportationMode.Boat && location.Visited)
            return BlipColor.DarkTurquoise;
         if(location.TransportationMode == TransportationMode.Boat && !location.Visited)
            return BlipColor.Turquoise;

         if(location.TransportationMode == TransportationMode.Ground && location.Visited)
            return BlipColor.DarkGreen;
         if(location.TransportationMode == TransportationMode.Ground && !location.Visited)
            return BlipColor.Green;

         if(location.TransportationMode == TransportationMode.Heli && location.Visited)
            return BlipColor.Purple;
         if(location.TransportationMode == TransportationMode.Heli && !location.Visited)
            return BlipColor.LightOrange;

         return BlipColor.Grey;
      }
   }
}