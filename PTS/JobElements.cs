﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cyron43.GtaIV.Common;
using GTA;
using GTA.Native;

namespace PTS
{
   internal class JobElements : Script
   {
      private static readonly object SyncLock = new object();

      #region static models

      private static readonly List<Model> StaticModels = new List<Model>
                                                         {
                                                            "M_O_MPMOBBOSS",
                                                            "M_Y_GOON_01",
                                                            "M_Y_VALET",
                                                            "F_O_RICH_01",
                                                            "F_M_PRICH_01",
                                                            "F_Y_PRICH_01",
                                                            "M_M_PRICH_01",
                                                            "M_Y_PRICH_01",
                                                            "F_M_BUSINESS_01",
                                                            "F_M_BUSINESS_02",
                                                            "F_M_PBUSINESS",
                                                            "F_Y_BUSINESS_01",
                                                            "M_M_BUSINESS_02",
                                                            "M_M_BUSINESS_03",
                                                            "M_M_PBUSINESS_01",
                                                            "M_Y_BUSINESS_01",
                                                            "M_Y_BUSINESS_02"
                                                         };

      #endregion

      private readonly Model _bodyguardModel = "M_Y_BOUNCER_02";
      private readonly Checkpoint _checkPoint;
      private int _attemptCounter;
      private Ped _bodyguard;
      private Blip _customerBlip;
      private bool _customerpickedUp;
      private string _customerType;
      private Location _destination;
      private Blip _destinationBlip;
      private Vector3 _destinationVector;
      private bool _hasDestination;
      private Location _pickupLocation;
      private Vector3 _pickupVector;
      private Blip _vehicleBlip;
      // ReSharper disable once UnusedMember.Global
      public JobElements()
      {
         //GTA.Script verlangt einen parameterlosen Constructor.
      }

      /// <summary>
      ///    Initializes a new instance of the <see cref="JobElements" /> class.
      /// </summary>
      /// <param name="checkPoint">An instance of the CheckPoint class.</param>
      /// <remarks>
      ///    I don't know why but for any reason the Checkpoint instance
      ///    has to be built in the Core class.
      /// </remarks>
      internal JobElements(Checkpoint checkPoint)
      {
         _checkPoint = checkPoint;
         _destination = new Location();
         JobHandler = new JobHandling();
         LocationHandling = new LocationHandling(this);
      }

      internal Vehicle CurrentVehicle { get; set; }
      internal Ped Customer { get; private set; }
      internal string CustomerModel { private get; set; }
      internal Blip HQBlip { get; set; }
      internal bool HQElementsEstablished { get; set; }
      internal bool HQLess { get; set; }
      internal Vehicle HQVehicle { get; private set; }
      internal bool IsOnDuty { get; private set; }
      internal JobHandling JobHandler { get; private set; }
      internal LocationHandling LocationHandling { get; private set; }
      internal bool ModIsActive { get; set; }
      internal ServiceState NextState { get; set; }
      internal VehicleSeat PassengerSeat { get; set; }
      internal string ServiceLocationsFullFilePath { get; set; }
      internal bool ShutdownRequested { private get; set; }
      internal bool SpawnBodyguard { private get; set; }
      internal bool TakeNearestLocationForNextJob { get; set; }
      internal Vector3 TransportHq { private get; set; }
      internal string VehicleModelName { private get; set; }
      internal Vector3 VehicleSpawnPosition { private get; set; }

      public void SoftShutDown()
      {
         if(HQLess)
         {
            ImmediateShutDown();
            return;
         }
         CreateTargetAtHQParkingSpot();
      }

      internal void CheckIfBackAtHQParkingSpot()
      {
         if(!(Game.LocalPlayer.Character.Position.DistanceTo(_destinationVector) < 10f)
            || !Game.LocalPlayer.Character.isInVehicle() ||
            !(Game.LocalPlayer.Character.CurrentVehicle.Speed < 1f))
            return;
         CommonFunctions.DisplayText("Leave the vehicle and go to the Limo HQ office desk (see necktie symbol).",
            5000);
         _destinationBlip.Delete();
         _checkPoint.Visible = false;
         NextState = ServiceState.BackAtParkingSpot;
      }

      internal void CheckIfBackAtOfficeDesk()
      {
         if(!(Game.LocalPlayer.Character.Position.DistanceTo(TransportHq) < 1f))
            return;
         FadeScreen(600);
         NextState = ServiceState.BackAtOfficeDesk;
      }

      internal void CheckIfJobDone()
      {
         var currentState = ShutdownRequested
            ? ServiceState.ShutDownRequested
            : ServiceState.HeadingForDestination;
         if(!CustomerIsInVehicle())
            AbortJob();
         if(!HQVehicleStillExistsWithHQMode())
            StartOverDueToLostVehicle(currentState);
         if(!CustomerDidNotLeaveTheVehiclePrematurely() || !CustomerIsAlive())
         {
            NextState = ServiceState.CustomerDeliveryFailed;
            return;
         }
         if(Customer.Position.DistanceTo(_destinationVector) < 10f && Game.LocalPlayer.Character.isInVehicle() &&
            Game.LocalPlayer.Character.CurrentVehicle.Speed < 1f)
         {
            LocationHandling.SaveLocationVisited(_destination);
            NextState = ServiceState.CustomerDelivered;
         }
      }

      internal void CreateCustomer()
      {
         lock(SyncLock)
         {
            Customer = World.CreatePed(RandomCustomerModelIfModelIsUndefined(), _pickupVector.ToGround());
            // Customer spawning can fail.
            if(!CommonFunctions.PedExists(Customer))
            {
               AbortJob();
               return;
            }
            Customer.MakeProofTo(false, false, false, true, true);
            Customer.BlockPermanentEvents = true;
            Customer.ChangeRelationship(RelationshipGroup.Player, Relationship.Respect);
            Customer.isRequiredForMission = true;
         }
      }

      internal void CreateJobIfDue()
      {
         RemoveVehicleBlipWhenPlayerIsInHQVehicleAndMakeItCurrentVehicle();
         DeleteHQVehicleIfHQLessModeActivated();
         if(!HQVehicleStillExistsWithHQMode())
            StartOverDueToLostVehicle(ServiceState.AwaitingJob);
         if(ShutdownRequested)
            return;
         if(!JobHandler.NextJobIsDue())
         {
            Game.DisplayText("Awaiting job...", 1000);
            return;
         }
         JobHandler.JobDone = false;
         if(!GetActualLocations())
         {
            ImmediateShutDown("There must be 2 locations at least in order to go on duty! PTS termitated.");
            return;
         }
         _pickupLocation = CreateNewPickup();
         _destination = CreateNewDestination();
         ChoosePersonWithHighestPriority();
         _customerBlip = BlipHandling.SetCustomerBlip(_pickupVector);
         _hasDestination = false;
         NextState = ServiceState.HeadingForCustomer;
         CommonFunctions.DisplayText(LocationHandling.CreateCustomerText(_pickupLocation), 5000);
      }

      internal void CreateVehicle(Model vehicleModel, Vector3 spawnPosition, float heading)
      {
         lock(SyncLock)
         {
            HQVehicle = World.CreateVehicle(vehicleModel, spawnPosition);
            if(HQVehicle == null)
               throw new ArgumentException(
                  string.Format("Vehicle {0} could not be created! Please check the model name.",
                     vehicleModel));
            HQVehicle.Dirtyness = 0.0f;
            HQVehicle.MakeProofTo(false, false, false, true, false);
            HQVehicle.Heading = heading;
         }
      }

      internal void CreateVehicleAtHQ(bool firstTime)
      {
         CreateVehicle(new Model(VehicleModelName), VehicleSpawnPosition, 117.93f);
         if(firstTime)
            HQVehicle.DoorLock = DoorLock.ImpossibleToOpen;
      }

      internal void CustomerPickup()
      {
         lock(SyncLock)
         {
            RemoveVehicleBlipWhenPlayerIsInHQVehicleAndMakeItCurrentVehicle();
            PassengerSeatExists();
            DeleteHQVehicleIfHQLessModeActivated();
            if(!HQVehicleStillExistsWithHQMode())
               StartOverDueToLostVehicle(ServiceState.HeadingForCustomer);
            Vector3 position;
            if(HQLess)
            {
               if(PlayerHasVehicle())
                  position = CurrentVehicle.Position;
               else
                  return;
            }
            else
               position = HQVehicle.Position;
            SetCheckPoint(_pickupVector, Color.Aquamarine);
            if(!(_pickupVector.DistanceTo(position) < 10.0f) || _customerpickedUp)
               return;
            CreateCustomer();
            if(LocationHandling.GetCurrentTransportationMode().Equals(TransportationMode.Heli))
               WaitUntilHeliHasLanded();
            MakeCustomerEnterVehicle();
         }
      }

      internal void DeleteHQElements()
      {
         HQBlip.Delete();
         if(_vehicleBlip != null)
            _vehicleBlip.Delete();
         HQElementsEstablished = false;
      }

      internal void FinishJob(int moneyEarned)
      {
         if(CommonFunctions.PedExists(Customer))
         {
            Customer.BlockPermanentEvents = false;
            Customer.ChangeRelationship(RelationshipGroup.Player, Relationship.Neutral);
            Customer.isRequiredForMission = false;
         }
         if(LocationHandling.GetCurrentTransportationMode().Equals(TransportationMode.Heli))
            WaitUntilHeliHasLanded();
         if(CommonFunctions.PedExists(Customer))
         {
            Customer.NoLongerNeeded();
            Customer.LeaveVehicle();
         }
         if(_destinationBlip != null)
            _destinationBlip.Delete();
         _checkPoint.Visible = false;
         _checkPoint.Disable();
         _customerpickedUp = false;
         Player.Money += moneyEarned;
         // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
         if(moneyEarned >= 0)
            Game.DisplayText(String.Format("Earned ${0}", moneyEarned), 2000);
         else
            Game.DisplayText(String.Format("Lost ${0}", -moneyEarned), 2000);
         JobHandler.JobDone = true;
         Wait(3000);
         JobHandler.CreateNextJobTime(ShutdownRequested);
         JobHandler.DisplayNextJobTimeIfRequested(ShutdownRequested);
         NextState = ShutdownRequested ? ServiceState.ShutdownApproved : ServiceState.AwaitingJob;
      }

      internal void ImmediateShutDown()
      {
         if(!ModIsActive)
         {
            CommonFunctions.DisplayText("Person Transport Service is inactive.", 2000);
            return;
         }
         DisposeObjects();
         IsOnDuty = false;
         _customerpickedUp = false;
         ModIsActive = false;
         ShutdownRequested = false;
         FadeScreen(600);
         CommonFunctions.DisplayText("Transport service is now deactivated.", 5000);
      }

      internal void ImmediateShutDown(string errorMessage)
      {
         CommonFunctions.DisplayText("~r~" + errorMessage + "~w~", 5000);
         DisposeObjects();
         IsOnDuty = false;
         _customerpickedUp = false;
         ModIsActive = false;
         ShutdownRequested = false;
         FadeScreen(600);
      }

      /// <summary>
      ///    Wenn der Zugang zum Fahrzeug für den Kunden blockiert ist, friert er ein.
      ///    Tasks werden akkumuliert. Um zu verhindern dass der Kunde ein- und wieder aussteigt
      ///    (was zu CustomerPrematurelyLeftTheVehicle führen würde),
      ///    muss die Task-Liste vor einem erneuten Kommando geleert werden.
      /// </summary>
      internal void MakeCustomerEnterVehicle()
      {
         if(!CommonFunctions.PedExists(Customer))
         {
            AbortJob();
            return;
         }
         if(CustomerIsInVehicle())
         {
            Customer.Task.AlwaysKeepTask = false;
            Customer.Invincible = false;
            NextState = ServiceState.WithCustomerNoDestination;
            return;
         }

         Customer.Invincible = true;
         Customer.Task.ClearAllImmediately();
         Customer.Task.EnterVehicle(CurrentVehicle, PassengerSeat);
         Customer.Task.AlwaysKeepTask = true;
         Customer.MakeProofTo(false, false, false, false, false);
         _checkPoint.Visible = false;
         _checkPoint.Disable();
         Game.DisplayText("Waiting up to 20 seconds for the customer...", 3000);
         NextState = ServiceState.CustomerEntersVehicle;
      }

      internal void PrepareForDestinationIfCustomerIsInLimo()
      {
         if(!HQVehicleStillExistsWithHQMode())
            StartOverDueToLostVehicle(ServiceState.WithCustomerNoDestination);
         if(!CustomerIsInVehicle() || _hasDestination)
            return;
         _customerBlip.Delete();
         _destinationBlip = BlipHandling.SetDestinationBlip(_destinationVector, _destination);
         SetCheckPoint(_destinationVector, Color.DarkMagenta);
         CommonFunctions.DisplayText(LocationHandling.CreateDestinationText(_destination, _customerType), 5000);
         _hasDestination = true;
         Customer.Invincible = false;
         LocationHandling.SaveLocationVisited(_pickupLocation);
         NextState = ServiceState.HeadingForDestination;
      }

      internal void PrepareForDuty()
      {
         if(HQLess)
         {
            PassengerSeatExists();
            GoOnDuty();
            return;
         }
         if(!PassengerSeatExists())
         {
            ImmediateShutDown();
            return;
         }
         if(!HQVehicleStillExistsWithHQMode())
            StartOverDueToLostVehicle(ServiceState.Start);
         if(IsOnDuty || !(Game.LocalPlayer.Character.Position.DistanceTo(TransportHq) < 1f))
            return;
         _vehicleBlip = BlipHandling.SpawnLimoBlip(HQVehicle);
         GoOnDuty();
      }

      internal void WaitForCustomerToEntervehicle()
      {
         lock(SyncLock)
         {
            if(_attemptCounter <= 21 && !CustomerIsInVehicle())
            {
               _attemptCounter++;
               Wait(1000);
               return;
            }
            if(_attemptCounter > 20 && !Customer.isGettingIntoAVehicle && !CustomerIsInVehicle())
            {
               _attemptCounter = 0;
               CurrentVehicle.CloseAllDoors();
               Customer.Task.AlwaysKeepTask = false;
               Customer.WarpIntoVehicle(CurrentVehicle, PassengerSeat);
            }
            _customerpickedUp = true;
            NextState = ServiceState.WithCustomerNoDestination;
         }
      }

      private static Model ConvertPedToModel(Ped ped)
      {
         var pedModel = ped.Model;
         ped.Delete();
         return pedModel;
      }

      private static Model StaticCustomerModel()
      {
         var ped = World.CreatePed(StaticModels[
            CommonFunctions.GetRandomIntegerNumber(0, (StaticModels.Count - 1))],
            new Vector3(0f, 0f, 0f));
         if(ped == null)
            return new Model();
         return ConvertPedToModel(ped);
      }

      private void AbortJob()
      {
         CommonFunctions.DisplayText("The customer had second thoughts and left. Await your next job...", 3000);
         if(_customerBlip != null)
            _customerBlip.Delete();
         FinishJob(0);
      }

      private void ChoosePersonWithHighestPriority()
      {
         _customerType = _destination.PersonPriority > _pickupLocation.PersonPriority
            ? _destination.PersonType
            : _pickupLocation.PersonType;
      }

      private Location CreateNewDestination()
      {
         lock(SyncLock)
         {
            var listPointer = CommonFunctions.GetRandomIntegerNumber(0, (LocationHandling.Locations.Count - 1));
            var location = LocationHandling.Locations[listPointer];
            _destinationVector.X = location.X;
            _destinationVector.Y = location.Y;
            _destinationVector.Z = location.Z;
            if(_destinationVector == _pickupVector)
               CreateNewDestination();
            return location;
         }
      }

      private Location CreateNewPickup()
      {
         Location location;
         if(TakeNearestLocationForNextJob)
            location = LocationHandling.GetNextToNearestLocation();
         else
         {
            var listPointer = CommonFunctions.GetRandomIntegerNumber(0, (LocationHandling.Locations.Count - 1));
            location = LocationHandling.Locations[listPointer];
         }
         _pickupVector.X = location.X;
         _pickupVector.Y = location.Y;
         _pickupVector.Z = location.Z;
         return location;
      }

      private void CreateTargetAtHQParkingSpot()
      {
         _destinationVector = VehicleSpawnPosition;
         if(_customerBlip != null)
            _customerBlip.Delete();
         _destination.TargetName = "Limo HQ parking spot";
         _destinationBlip = BlipHandling.SetDestinationBlip(_destinationVector, _destination);
         SetCheckPoint(_destinationVector, Color.Gold);
         CommonFunctions.DisplayText("Drive back to the HQ parking spot.", 5000);
         NextState = ServiceState.HeadingBackToHQ;
      }

      private bool CustomerDidNotLeaveTheVehiclePrematurely()
      {
         if(!CommonFunctions.PedExists(Customer))
         {
            return false;
         }
         /* Wieder was gelernt: Bei solchen kombinierten Prüfungen wird diese abgebrochen,
          * wenn der erste Teil schon false zurück gibt. Das führte dazu, dass CustomerIsInVehicle
          * nicht mehr aufgerufen wird, wenn _customerpickedUp auf false gegangen ist
          * (Prüfung war vorher anders herum)! */
         if(!CustomerIsInVehicle() && _customerpickedUp)
         {
            DeliveryFailed("The customer had enough of your bad driving and left!");
            return false;
         }
         return true;
      }

      private bool CustomerIsAlive()
      {
         if(!CommonFunctions.PedExists(Customer))
            return false;
         if(!Customer.isAlive)
         {
            DeliveryFailed("The ~b~customer~w~ died! ~r~Job failed.~w~");
            return false;
         }
         return true;
      }

      private bool CustomerIsInVehicle()
      {
         return CurrentVehicle != null
                && Customer != null
                && Customer.isSittingInVehicle(CurrentVehicle);
      }

      private void DeleteHQVehicleIfHQLessModeActivated()
      {
         if(!HQLess)
            return;
         if(CommonFunctions.VehicleExists(HQVehicle))
            HQVehicle.Delete();
      }

      private void DeliveryFailed(string reason)
      {
         _customerBlip.Delete();
         _destinationBlip.Delete();
         _customerpickedUp = false;
         Customer.NoLongerNeeded();
         CommonFunctions.DisplayText(reason, 5000);
         Wait(5000);
         JobHandler.JobDone = true;
         JobHandler.CreateNextJobTime(ShutdownRequested);
         JobHandler.DisplayNextJobTimeIfRequested(ShutdownRequested);
      }

      private void DisposeObjects()
      {
         _checkPoint.Visible = false;
         _checkPoint.Disable();
         if(_vehicleBlip != null)
            _vehicleBlip.Delete();
         if(_customerBlip != null)
            _customerBlip.Delete();
         if(CommonFunctions.PedExists(Customer))
         {
            if(Customer.isInVehicle())
               Customer.LeaveVehicle();
            Customer.NoLongerNeeded();
         }
         if(_destinationBlip != null)
            _destinationBlip.Delete();
         ReleaseBodyguard();
         if(HQBlip != null)
            HQBlip.Delete();
         if(HQVehicle != null)
            HQVehicle.Delete();
         CurrentVehicle = null;
      }

      private void FadeScreen(int milliseconds)
      {
         Function.Call("DO_SCREEN_FADE_OUT");
         Wait(milliseconds);
         Function.Call("DO_SCREEN_FADE_IN");
      }

      /// <summary>
      ///    Gets the actual locations.
      /// </summary>
      /// <returns><c>false</c> if ImmediateShutdown was called because of an insufficient amount of locations.</returns>
      private bool GetActualLocations()
      {
         LocationHandling.CreateListOfCurrentlyAvailableLocations(
            World.CurrentDayTime, World.CurrentDate.DayOfWeek, ServiceLocationsFullFilePath);
         return LocationHandling.Locations.Count >= 2;
      }

      private void GoOnDuty()
      {
         FadeScreen(1000);
         IsOnDuty = true;
         SpawnBodyguardIfRequested();
         JobHandler.CreateNextJobTime(ShutdownRequested);
         JobHandler.JobDone = true;
         NextState = ServiceState.AwaitingJob;
         CommonFunctions.DisplayText("You are now on duty. Await your order...", 5000);
         JobHandler.DisplayNextJobTimeIfRequested(ShutdownRequested);
      }

      private bool HQVehicleStillExistsWithHQMode()
      {
         if(HQLess)
            return true;
         if(HQVehicle == null || !Game.Exists(HQVehicle) || !HQVehicle.isAlive)
         {
            return false;
         }
         return true;
      }

      private bool PassengerSeatExists()
      {
         var passengerSeatExists = true;
         if(HQLess && PlayerHasVehicle())
         {
            if(CurrentVehicle.PassengerSeats < 2
               && PassengerSeat != VehicleSeat.RightFront
               && PassengerSeat != VehicleSeat.AnyPassengerSeat)
               passengerSeatExists = false;
         }
         if(!HQLess)
         {
            if(HQVehicle.PassengerSeats < 2
               && PassengerSeat != VehicleSeat.RightFront
               && PassengerSeat != VehicleSeat.AnyPassengerSeat)
               passengerSeatExists = false;
         }
         if(!passengerSeatExists)
            Game.DisplayText(
               "The passenger seat you defined does not exist in the current vehicle!", 3000);
         return passengerSeatExists;
      }

      private bool PlayerHasVehicle()
      {
         CurrentVehicle = Player.Character.CurrentVehicle;
         return CurrentVehicle != null;
      }

      private bool PlayerIsInHQVehicle()
      {
         if(HQVehicle == null)
            return false;
         return Game.LocalPlayer.Character.isInVehicle(HQVehicle);
      }

      private Model RandomCustomerModelIfModelIsUndefined()
      {
         if(!String.IsNullOrEmpty(CustomerModel))
            return CustomerModel;
         if(_customerType != "customer")
            return StaticCustomerModel();
         var ped = World.CreatePed(_pickupVector);
         return ped == null || !ped.Exists() ? new Model() : ConvertPedToModel(ped);
      }

      private void ReleaseBodyguard()
      {
         if(_bodyguard == null || !Game.Exists(_bodyguard))
            return;
         _bodyguard.NoLongerNeeded();
         _bodyguard.LeaveGroup();
      }

      private void RemoveVehicleBlipWhenPlayerIsInHQVehicleAndMakeItCurrentVehicle()
      {
         if(HQVehicle == null || !Game.Exists(HQVehicle))
            return;

         if(PlayerIsInHQVehicle())
         {
            if(_vehicleBlip != null)
               _vehicleBlip.Delete();
            CurrentVehicle = HQVehicle;
         }
      }

      private void SetCheckPoint(Vector3 position, Color color)
      {
         _checkPoint.Position = position.ToGround();
         _checkPoint.Color = color;
         _checkPoint.Diameter = 1f;
         _checkPoint.Visible = true;
      }

      private void SpawnBodyguardIfRequested()
      {
         if(!SpawnBodyguard)
            return;
         if(HQLess)
            _bodyguard = World.CreatePed(_bodyguardModel, Player.Character.Position.Around(2.0f));
         else
         {
            _bodyguard = World.CreatePed(_bodyguardModel, TransportHq);
            CurrentVehicle = HQVehicle;
         }
         _bodyguard.Weapons.MP5.Ammo = 30000;
         _bodyguard.WillDoDrivebys = true;
         _bodyguard.RelationshipGroup = RelationshipGroup.Player;
         _bodyguard.ChangeRelationship(0, 0);
         _bodyguard.CantBeDamagedByRelationshipGroup(0, false);
         Game.LocalPlayer.Group.AddMember(_bodyguard);
         _bodyguard.Task.EnterVehicle(CurrentVehicle, VehicleSeat.RightFront);
      }

      private void StartOverDueToLostVehicle(ServiceState currentState)
      {
         CommonFunctions.DisplayText("~r~Dude what have you done to the vehicle???~w~", 3000);
         Wait(3000);
         if(HQLess)
         {
            CommonFunctions.DisplayText("Get a new vehicle.", 3000);
            Wait(3000);
         }
         else
         {
            CommonFunctions.DisplayText("Get a new vehicle at the HQ.", 3000);
            Wait(3000);
            CreateVehicleAtHQ(firstTime: false);
         }
         NextState = currentState;
      }

      private void WaitUntilHeliHasLanded()
      {
         while(!CurrentVehicle.isOnAllWheels)
            Wait(25);
      }
   }
}