﻿using System;

namespace PTS
{
   public class JobContainer
   {
      internal Delegate Job { get; set; }
      internal object[] Parameters { get; set; }
   }
}