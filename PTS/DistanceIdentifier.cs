﻿namespace PTS
{
   internal class DistanceIdentifier
   {
      internal int LocationId { get; set; }
      internal float Distance { get; set; }
   }
}