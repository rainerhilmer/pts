﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   public class LocationCapture : Script
   {
      private readonly XmlIo _fileHandler;
      private readonly string _fullPath;
      private readonly LocationHandling _locationHandling;
      private List<Location> _locations;

      public LocationCapture()
      {
         //GTA.Script verlangt einen parameterlosen Constructor.
      }

      public LocationCapture(string fullServiceLocationsPath)
      {
         _fullPath = fullServiceLocationsPath;
         _fileHandler = new XmlIo(_fullPath);
         _locations = new List<Location>();
         _locationHandling = new LocationHandling(new JobElements(new Checkpoint())
                                                  {
                                                     ServiceLocationsFullFilePath =
                                                        CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml"
                                                  });
      }

      internal void SaveNewLocation()
      {
         var location = CreateNewLocation();
         if(!File.Exists(_fullPath))
         {
            location.Id = 1;
            _locations.Add(location);
            _fileHandler.Save(_locations);
            return;
         }
         _locations.Clear();
         _locations = _fileHandler.Load<List<Location>>();
         RearrangeLocationIDs();
         location.Id = _locations.Count + 1;
         _locations.Add(location);
         ReplaceTargetNameIfEqualsStreetName();
         _fileHandler.Save(_locations);
         _locationHandling.GetPreparedLocationList(_fullPath);
      }

      private Location CreateNewLocation()
      {
         var openDays = new List<DayOfWeek>
                        {
                           DayOfWeek.Monday,
                           DayOfWeek.Tuesday,
                           DayOfWeek.Wednesday,
                           DayOfWeek.Thursday,
                           DayOfWeek.Friday,
                           DayOfWeek.Saturday,
                           DayOfWeek.Sunday
                        };
         return new Location
                {
                   OpenDays = openDays,
                   AvailableFrom = 0,
                   AvailableUntil = 24,
                   PersonPriority = 0,
                   PersonType = "customer",
                   StreetName = World.GetStreetName(Player.Character.Position),
                   TargetName = ".",
                   TransportationMode = _locationHandling.GetCurrentTransportationMode(),
                   Visited = false,
                   X = Player.Character.Position.X,
                   Y = Player.Character.Position.Y,
                   Z = Player.Character.Position.Z
                };
      }

      private void RearrangeLocationIDs()
      {
         for(var i = 0; i < _locations.Count; i++)
         {
            _locations[i].Id = i + 1;
         }
      }

      private void ReplaceTargetNameIfEqualsStreetName()
      {
         foreach(var location in _locations.Where(location => location.TargetName.Equals(location.StreetName)))
         {
            location.TargetName = ".";
         }
      }
   }
}