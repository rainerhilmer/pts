﻿using GTA;

namespace PTS
{
   internal class StateObjects
   {
      internal ServiceState CurrentState { get; set; }
      internal Ped Customer { get; set; }
      internal Vector3 CurrentPlayerVector { get; set; }
      internal Vehicle CurrentVehicle { get; set; }
   }
}