﻿using GTA;

namespace PTS
{
   internal class Failover : Script
   {
      private readonly JobElements _jobElements;

      internal Failover(JobElements jobElements)
      {
         _jobElements = jobElements;
      }

      public Failover()
      {
         //GTA.Script verlangt einen parameterlosen Constructor.
      }

      internal void Recover(StateObjects stateObjects)
      {
         if(stateObjects.CurrentState == ServiceState.AwaitingJob)
            RecoverVehicle(stateObjects.CurrentVehicle, stateObjects.CurrentPlayerVector);

         if(stateObjects.CurrentState == ServiceState.HeadingForCustomer)
         {
            RecoverCustomer(stateObjects.Customer);
            RecoverVehicle(stateObjects.CurrentVehicle, stateObjects.CurrentPlayerVector);
         }

         if(stateObjects.CurrentState == ServiceState.CustomerEntersVehicle)
            RecoverCustomer(stateObjects.Customer);
      }

      private void RecoverCustomer(Ped customer)
      {
         if(customer == null)
            return;
         if(!Game.Exists(customer))
            return;
         customer.NoLongerNeeded();
         customer.Detach();
         customer.Delete();
         _jobElements.CreateCustomer();
         _jobElements.MakeCustomerEnterVehicle();
      }

      private void RecoverVehicle(Vehicle vehicle, Vector3 objectVector)
      {
         if(_jobElements.HQLess)
            return;
         var currentVehicleModel = vehicle.Model;
         var spawnPosition = objectVector.Around(3.0f).ToGround();
         vehicle.Delete();
         _jobElements.CreateVehicle(currentVehicleModel, spawnPosition, Player.Character.Heading);
         _jobElements.HQVehicle.PlaceOnNextStreetProperly();
         _jobElements.HQVehicle.FreezePosition = false;
         _jobElements.HQVehicle.EngineHealth = 1000;
         _jobElements.HQVehicle.EngineRunning = true;
         _jobElements.HQVehicle.Health = 1000;
         _jobElements.HQVehicle.Dirtyness = 0;
      }
   }
}