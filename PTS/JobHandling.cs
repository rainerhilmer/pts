﻿using System;
using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   internal class JobHandling
   {
      private TimeSpan _nextJobAt;
      internal bool JobDone;
      internal bool DisplayTimeOfNextJob { get; set; }
      internal uint JobDelayMax { get; set; }
      internal uint JobDelayMin { get; set; }

      internal void CreateNextJobTime(bool shutdownRequested)
      {
         if(shutdownRequested)
            return;
         var currentDayTime = World.CurrentDayTime;
         _nextJobAt = new TimeSpan(
            currentDayTime.Hours,
            currentDayTime.Minutes + CommonFunctions.GetRandomIntegerNumber((int) JobDelayMin, (int) JobDelayMax),
            currentDayTime.Seconds);
         CommonFunctions.DisplayText("Await your next order.", 5000);
      }

      internal void DisplayNextJobTimeIfRequested(bool shutdownRequested)
      {
         if(DisplayTimeOfNextJob && !shutdownRequested)
            CommonFunctions.DisplayText("Next job will be at " + _nextJobAt, 5000);
      }

      internal bool NextJobIsDue()
      {
         return JobDone && World.CurrentDayTime >= _nextJobAt;
      }
   }
}