﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   internal class LocationHandling : Script
   {
      private static readonly object SyncLock = new object();
      private readonly JobElements _jobElements;

      public LocationHandling(JobElements jobElements)
      {
         _jobElements = jobElements;
         Locations = new List<Location>();
      }

      public LocationHandling()
      {
         Locations = new List<Location>();
      }

      internal bool IgnoreOpenDays { private get; set; }
      internal bool IgnoreOpenHours { private get; set; }

      /// <summary>
      ///    Gets the locations.
      /// </summary>
      /// <value>
      ///    The locations.
      /// </value>
      internal List<Location> Locations { get; private set; }

      internal static string CreateCustomerText(Location pickupLocation)
      {
         return String.Format("Pick up a ~b~{0}~w~ at ~y~{1}~w~.",
            pickupLocation.PersonType, pickupLocation.TargetName);
      }

      internal static string CreateDestinationText(Location destination, string customerType)
      {
         return String.Format("Drive the {0} to ~p~{1}~w~.", customerType, destination.TargetName);
      }

      internal static bool IsWithinOpeningHours(byte availableFrom, byte availableUntil, TimeSpan currentTime)
      {
         var openFrom = new TimeSpan(availableFrom, 0, 0);
         var openUntil = new TimeSpan(availableUntil, 0, 0);
         if(openUntil <= openFrom)
            openUntil += new TimeSpan(24, 0, 0);
         if(currentTime < openFrom)
            currentTime += new TimeSpan(24, 0, 0);
         return currentTime >= openFrom && currentTime <= openUntil;
      }

      internal static List<Location> TruncateLongLocationNames(List<Location> locations, int maxLength)
      {
         foreach(var location in locations.Where(location => location.TargetName.Length > maxLength))
         {
            location.TargetName = location.TargetName.Substring(0, maxLength - 3) + "...";
         }
         return locations;
      }

      internal void CreateListOfCurrentlyAvailableLocations(
         TimeSpan currentDayTime, DayOfWeek currentDay, string fullPath)
      {
         var correctCurrentDay = CommonFunctions.CorrectedCurrentDay(currentDay);
         GetPreparedLocationList(fullPath);
         if(Locations == null)
         {
            _jobElements.ImmediateShutDown("Location-list could not be read.");
            return;
         }
         ResetVisitedIfLessThan2();
         var currentlyAvailableLocations = new List<Location>();
         foreach(var location in Locations.Where(location => !location.Visited))
         {
            if(!IgnoreOpenDays && !IgnoreOpenHours)
            {
               if(IsWithinOpenDays(location.OpenDays, correctCurrentDay)
                  && IsWithinOpeningHours(location.AvailableFrom, location.AvailableUntil, currentDayTime))
                  currentlyAvailableLocations.Add(location);
            }

            if(!IgnoreOpenDays && IgnoreOpenHours)
            {
               if(IsWithinOpenDays(location.OpenDays, correctCurrentDay))
                  currentlyAvailableLocations.Add(location);
            }

            if(IgnoreOpenDays && !IgnoreOpenHours)
            {
               if(IsWithinOpeningHours(location.AvailableFrom, location.AvailableUntil, currentDayTime))
                  currentlyAvailableLocations.Add(location);
            }

            if(IgnoreOpenDays && IgnoreOpenHours)
            {
               currentlyAvailableLocations.Add(location);
            }
         }
         Locations = currentlyAvailableLocations;
      }

      internal TransportationMode GetCurrentTransportationMode()
      {
         if(Player.LastVehicle == null)
            return TransportationMode.Ground;
         if(Player.Character.isSittingInVehicle() && Player.Character.CurrentVehicle.Model.isBoat)
            return TransportationMode.Boat;
         if(Player.Character.isSittingInVehicle() && Player.Character.CurrentVehicle.Model.isHelicopter)
            return TransportationMode.Heli;
         if(!Exists(Player.LastVehicle) || !_jobElements.HQLess)
            return TransportationMode.Ground;
         if(!Player.Character.isSittingInVehicle() && Player.LastVehicle.Model.isBoat)
            return TransportationMode.Boat;
         if(!Player.Character.isSittingInVehicle() && Player.LastVehicle.Model.isHelicopter)
            return TransportationMode.Heli;
         return TransportationMode.Ground;
      }

      /// <summary>
      ///    Der am nächsten gelegene Punkt ist logischerweise der,
      ///    auf dem der Kunde gerade abgeliefert wurde.
      ///    Es muss also der Zweitnächste gesucht werden.
      /// </summary>
      internal Location GetNextToNearestLocation()
      {
         lock(SyncLock)
         {
            var distanceIdentifiers = GetDistanceIdentifiers();
            var nearestDistanceIdentifier = GetNearestDistanceIdentifier(ref distanceIdentifiers);
            distanceIdentifiers.Remove(nearestDistanceIdentifier);
            nearestDistanceIdentifier = GetNearestDistanceIdentifier(ref distanceIdentifiers);
            return Locations.Find(l => l.Id == nearestDistanceIdentifier.LocationId);
         }
      }

      /// <summary>
      ///    Reads the location list which is provided through the
      ///    <see
      ///       cref="Locations" />
      ///    property.
      /// </summary>
      /// <remarks>
      ///    If no location name is set, the location will get the street name.
      ///    Long location names are getting truncated. <see cref="Locations" />
      ///    contains locations matching the current transportation mode only!
      /// </remarks>
      /// <param name="fullPath">The full path to PTSLocations.xml.</param>
      internal void GetPreparedLocationList(string fullPath)
      {
         Locations.Clear();
         Locations = new XmlIo(fullPath)
            .Load<List<Location>>();
         PrepareLocationListForPlaying();
      }

      /// <summary>
      ///    Resets all Location.Visited flags to false and saves them to
      ///    PTSLocations.xml.
      /// </summary>
      internal void ResetVisited()
      {
         if(Locations.Count == 0)
            ReadFullLocationList(CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
         foreach(var location in Locations)
         {
            location.Visited = false;
         }
         SaveLocationList(Locations, CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
         Game.DisplayText("All locations have been reset to not visited.", 5000);
      }

      /// <summary>
      ///    Overwrites the actual location marked as visited and saves the
      ///    whole location list.
      /// </summary>
      /// <param name="location">The ID of the location.</param>
      internal void SaveLocationVisited(Location location)
      {
         ReadFullLocationList(CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
         Location loc;
         try
         {
            loc = Locations.Find(x => x.Id == location.Id);
         }
         catch(ArgumentNullException)
         {
            Game.DisplayText("PTS information: Visited location could not be found in the list.", 3000);
            PrepareLocationListForPlaying();
            return;
         }
         var index = Locations.IndexOf(loc);
         if(index < 0 || index > Locations.Count - 1)
         {
            Game.DisplayText("PTS information: Index error("
                             + index
                             + "). Location ID"
                             + location
                             + " could not be marked as visited.",
               3000);
            return;
         }
         Locations[index].Visited = true;
         SaveLocationList(
            Locations,
            CommonFunctions.FileRepositoryPath + @"\PTSLocations.xml");
         PrepareLocationListForPlaying();
      }

      private List<Location> ExtractLocationsMatchingTransportationMode(IEnumerable<Location> locations)
      {
         return locations.Where(location => location.TransportationMode.Equals(GetCurrentTransportationMode())).ToList();
      }

      private List<DistanceIdentifier> GetDistanceIdentifiers()
      {
         return Locations.Select(location => new DistanceIdentifier
                                             {
                                                Distance = Player.Character.Position
                                                   .DistanceTo(new Vector3(location.X, location.Y, location.Z)),
                                                LocationId = location.Id
                                             }).ToList();
      }

      private static DistanceIdentifier GetNearestDistanceIdentifier(ref List<DistanceIdentifier> distanceIdentifiers)
      {
         var tempIdentifier = new DistanceIdentifier {Distance = float.MaxValue};
         foreach(var identifier in distanceIdentifiers)
         {
            if(identifier.Distance < tempIdentifier.Distance)
               tempIdentifier = identifier;
         }
         return tempIdentifier;
      }

      private static bool IsWithinOpenDays(IEnumerable<DayOfWeek> openDays, DayOfWeek currentDay)
      {
         return openDays.Any(d => d == currentDay);
      }

      private void PrepareLocationListForPlaying()
      {
         Locations = TakeStreetNameIfLocationNameIsNotSet(Locations);
         Locations = TruncateLongLocationNames(Locations, 20);
         Locations = ExtractLocationsMatchingTransportationMode(Locations);
      }

      private void ReadFullLocationList(string fullPath)
      {
         Locations = new XmlIo(fullPath)
            .Load<List<Location>>();
      }

      /// <summary>
      ///    Resets the visited tag of all locations if less than 2 locations are not visited.
      /// </summary>
      /// <note>Two locations (pickup and destination) are needed with each new job.</note>
      private void ResetVisitedIfLessThan2()
      {
         var counter = 0;
         foreach(var location in Locations)
         {
            if(!location.Visited)
               counter++;
            if(counter >= 2)
               return;
         }
         ResetVisited();
      }

      private static void SaveLocationList(List<Location> targetList, string fullPath)
      {
         new XmlIo(fullPath).Save(targetList);
      }

      private static List<Location> TakeStreetNameIfLocationNameIsNotSet(List<Location> locations)
      {
         foreach(var location in locations)
         {
            if(location.TargetName == "placeholder"
               || location.TargetName == "."
               || string.IsNullOrEmpty(location.TargetName))
            {
               location.TargetName = location.StreetName;
            }
         }
         return locations;
      }
   }
}