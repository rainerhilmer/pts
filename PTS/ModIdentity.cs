﻿using Cyron43.GtaIV.Common;

namespace PTS
{
   internal class ModIdentity : IModIdentity
   {
      public string AssemblyName { get; set; }
      public string FullPath { get; set; }
      public int Version { get; set; }
   }
}