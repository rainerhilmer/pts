﻿using System;
using System.IO;

namespace PTS
{
   /// <summary>
   ///    Bietet Funktionen für das Schreiben und Lesen von Logfiles im Textformat.
   /// </summary>
   /// <example>
   ///    <code>
   ///  <![CDATA[
   /// using DoNeX.Logging;
   /// 
   /// namespace Test
   /// {
   ///    class Program
   ///    {
   ///       #region Private Methods
   /// 
   ///       static void Main()
   ///       {
   ///          Logger log = new TextLogger(@"X:\MyLogs\Log.txt");
   ///          if(log.Exists)
   ///             log.Delete();
   ///          log.Create("Test");
   ///          log.Write("Bla");
   ///       }
   /// 
   ///       #endregion
   ///    }
   /// }
   ///  ]]>
   ///  </code>
   /// </example>
   public sealed class TextLogger : Logger
   {
      #region Fields

      /// <summary>
      ///    FileInfo instance
      /// </summary>
      private FileInfo _fileInfo;

      #endregion Fields

      #region Constructors

      /// <summary>
      ///    Initialisiert eine neue Instanz der <code>TextLogger</code>-Klasse
      /// </summary>
      /// <param name="fullLogname">Der volle Pfad und Filename des Logs ohne Extension.</param>
      public TextLogger(string fullLogname)
      {
         FullLogName = fullLogname;
         _fileInfo = new FileInfo(FullLogName);
      }

      #endregion Constructors

      #region Public Methods

      /// <summary>
      ///    Erstellt ein neues Logfile unter dem im Konstruktor der Klasse angegebenen
      ///    Pfad und Namen.
      /// </summary>
      /// <param name="title">Die Titelzeile im Logfile.</param>
      /// <exception cref="IOException">
      ///    Wirft eine IOException
      ///    falls das Logfile bereits existiert.
      /// </exception>
      public override void Create(string title)
      {
         var underline = "";
         for(var i = 0; i < title.Length; i++)
         {
            underline += "=";
         }
         if(!_fileInfo.Exists)
         {
            using(var sw = _fileInfo.CreateText())
            {
               sw.WriteLine(title);
               sw.WriteLine(underline);
               sw.WriteLine();
            }

            //fi.Exists bleibt sonst auf false stehen.
            _fileInfo.Refresh();
         }
      }

      /// <summary>
      ///    Löscht das im Konstruktor der Klasse angegebene Logfile.
      /// </summary>
      public override void Delete()
      {
         if(_fileInfo.Exists)
         {
            _fileInfo.Delete();

            // Sonst bleibt das fi.Exists auf true stehen:
            _fileInfo.Refresh();
         }
      }

      /// <summary>
      ///    Gibt Resourcen frei.
      /// </summary>
      public override void Dispose(bool disposing)
      {
         if(disposing)
         {
            // Freigeben von managed objects.
            _fileInfo = null;
         }
         base.Dispose(disposing);
      }

      /// <summary>
      ///    Liest das im Konstruktor der TextLogger-Klasse angegebene Logfile
      ///    komplett in den Speicher.
      /// </summary>
      /// <returns>Das Logfile</returns>
      public override string Read()
      {
         using(var sr = _fileInfo.OpenText())
         {
            return sr.ReadToEnd();
         }
      }

      /// <summary>
      ///    Schreibt eine neue Zeile in das Logfile.
      /// </summary>
      /// <param name="entry">Der Logeintrag</param>
      /// <param name="actualDateAndTime">Das aktuelle Datum und die Uhrzeit</param>
      /// <exception cref="IOException">
      ///    Wirft eine IOException
      ///    falls das Logfile nicht existiert.
      /// </exception>
      public override void Write(string entry, DateTime actualDateAndTime)
      {
         if(_fileInfo.Exists)
         {
            using(var sw = _fileInfo.AppendText())
            {
               sw.Write(DateTime.Now + ": ");
               sw.WriteLine(entry);
            }
         }
         else
            throw new IOException("Logfile existiert nicht!");
      }

      /// <summary>
      ///    Schreibt eine neue Zeile in das Logfile.
      /// </summary>
      /// <param name="entry">Der Logeintrag</param>
      /// <exception cref="IOException">
      ///    Wirft eine IOException
      ///    falls das Logfile nicht existiert.
      /// </exception>
      public override void Write(string entry)
      {
         if(_fileInfo.Exists)
         {
            using(var sw = _fileInfo.AppendText())
            {
               sw.WriteLine(entry);
            }
         }
         else
            throw new IOException("Logfile existiert nicht!");
      }

      #endregion Public Methods
   }
}