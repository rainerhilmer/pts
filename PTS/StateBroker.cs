﻿using System;
using System.Collections.Generic;

namespace PTS
{
   public class StateBroker
   {
      private readonly Dictionary<ServiceState, JobContainer> _jobDictionary;
      private readonly JobElements _jobs;

      internal StateBroker(JobElements jobs)
      {
         _jobs = jobs;
         _jobDictionary = new Dictionary<ServiceState, JobContainer>();
      }

      internal int FineOnDeath { get; set; }
      internal int FineOnInjury { get; set; } //todo: Injury Fine berücksichtigen.
      internal int Income { get; set; }

      internal void RegisterJob(ServiceState state, JobContainer job)
      {
         _jobDictionary.Add(state, job);
      }

      internal void RegisterJobs()
      {
         RegisterJob(ServiceState.Start,
            new JobContainer {Job = new Action(_jobs.PrepareForDuty)});

         RegisterJob(ServiceState.AwaitingJob,
            new JobContainer {Job = new Action(_jobs.CreateJobIfDue)});

         RegisterJob(ServiceState.HeadingForCustomer,
            new JobContainer {Job = new Action(_jobs.CustomerPickup)});

         RegisterJob(ServiceState.CustomerEntersVehicle,
            new JobContainer {Job = new Action(_jobs.WaitForCustomerToEntervehicle)});

         RegisterJob(ServiceState.WithCustomerNoDestination,
            new JobContainer {Job = new Action(_jobs.PrepareForDestinationIfCustomerIsInLimo)});

         RegisterJob(ServiceState.HeadingForDestination,
            new JobContainer {Job = new Action(_jobs.CheckIfJobDone)});

         RegisterJob(ServiceState.CustomerDelivered,
            new JobContainer
            {
               Job = new Action<int>(_jobs.FinishJob),
               Parameters = new object[] {Income}
            });

         RegisterJob(ServiceState.CustomerDeliveryFailed,
            new JobContainer
            {
               Job = new Action<int>(_jobs.FinishJob),
               Parameters = new object[] {FineOnDeath}
            });

         RegisterJob(ServiceState.ShutDownRequested,
            new JobContainer {Job = new Action(_jobs.CheckIfJobDone)});

         RegisterJob(ServiceState.ShutdownApproved,
            new JobContainer {Job = new Action(_jobs.SoftShutDown)});

         RegisterJob(ServiceState.HeadingBackToHQ,
            new JobContainer {Job = new Action(_jobs.CheckIfBackAtHQParkingSpot)});

         RegisterJob(ServiceState.BackAtParkingSpot,
            new JobContainer {Job = new Action(_jobs.CheckIfBackAtOfficeDesk)});

         RegisterJob(ServiceState.BackAtOfficeDesk,
            new JobContainer {Job = new Action(_jobs.ImmediateShutDown)});
      }

      public void RunJob(ServiceState state)
      {
         if(_jobDictionary[state].Parameters == null)
            _jobDictionary[state].Job.DynamicInvoke();
         else
            _jobDictionary[state].Job.DynamicInvoke(_jobDictionary[state].Parameters);
      }
   }
}