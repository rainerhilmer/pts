﻿using Cyron43.GtaIV.Common;
using GTA;

namespace PTS
{
   internal class KeyHandling : Script
   {
      private readonly Core _core;
      // ReSharper disable once UnusedMember.Global
      public KeyHandling()
      {
         //GTA.Script verlangt einen parameterlosen Constructor.
      }

      public KeyHandling(Core core)
      {
         _core = core;
      }

      internal void HandleKey(KeyEventArgs e)
      {
         AtRequestStartupOrShutdownKey(e);
         if(_core.TypeOfError != ErrorType.None)
            return;
         AtFailoverKey(e);
         AtHQLessKey(e);
         AtImmediateShutdownKey(e);
         AtLocationMapKey(e);
         AtResetVisited(e);
         AtSaveNewLocationKey(e);
      }

      private void AtFailoverKey(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "Failover"), e))
            return;
         _core.Status.Customer = _core.JobElements.Customer;
         _core.Status.CurrentState = _core.JobElements.NextState;
         _core.Status.CurrentPlayerVector = Player.Character.Position;
         _core.Status.CurrentVehicle = _core.JobElements.CurrentVehicle;
         new Failover(_core.JobElements).Recover(_core.Status);
      }

      private void AtHQLessKey(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "HQLessMode"), e))
            return;
         if(!_core.JobElements.HQLess && !Player.Character.isInVehicle())
         {
            CommonFunctions.DisplayText("PTS change duty mode: Enter a "
                                        + "vehicle and try again.", 3000);
            return;
         }
         if(!_core.JobElements.HQLess)
         {
            if(_core.JobElements.HQElementsEstablished)
               _core.JobElements.DeleteHQElements();
            _core.JobElements.HQLess = true;
            _core.HQLessModeManuallySwitched = true;
            _core.JobElements.CurrentVehicle = Player.Character.CurrentVehicle;
            CommonFunctions.DisplayText("PTS HQ-less mode now established.", 5000);
            return;
         }
         if(_core.JobElements.HQLess && _core.JobElements.IsOnDuty)
         {
            CommonFunctions.DisplayText("PTS: You cannot switch back to "
                                        + "regular mode while you are on duty.", 5000);
            return;
         }
         _core.JobElements.HQLess = false;
         _core.HQLessModeManuallySwitched = true;
         CommonFunctions.DisplayText("PTS HQ mode established.", 5000);
      }

      private void AtImmediateShutdownKey(KeyEventArgs e)
      {
         // Nicht Alt+X benutzen. Der key ist belegt und bringt das Spiel zum Absturz!
         if(
            !KeyHandlingCommons.KeyIs(
               KeyHandlingCommons.GetKeyContainer(
                  _core.Configuration.Keys, "ImmediateShutdown"), e))
            return;
         _core.JobElements.ImmediateShutDown();
         _core.Tick -= _core.OnTick;
      }

      private void AtLocationMapKey(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "LocationMap"), e))
            return;
         BlipHandling.TriggerLocationMap(
            _core.JobElements.LocationHandling,
            _core.JobElements.ServiceLocationsFullFilePath,
            Player.Character.Position);
      }

      private void AtRequestStartupOrShutdownKey(KeyEventArgs e)
      {
         if(
            !KeyHandlingCommons.KeyIs(
               KeyHandlingCommons.GetKeyContainer(
                  _core.Configuration.Keys, "RequestStartupOrShutdown"), e))
            return;
         if(!_core.JobElements.ModIsActive)
         {
            _core.Trigger();
            return;
         }
         _core.RequestShutdown();
      }

      private void AtResetVisited(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "ResetVisited"), e))
            return;
         _core.JobElements.LocationHandling.ResetVisited();
      }

      private void AtSaveNewLocationKey(KeyEventArgs e)
      {
         if(
            !KeyHandlingCommons.KeyIs(
               KeyHandlingCommons.GetKeyContainer(
                  _core.Configuration.Keys, "SaveNewLocation"), e))
            return;
         if(!_core.Configuration.EnableSaveNewLocation)
         {
            CommonFunctions.DisplayText("Saving new locations is disabled.", 3000);
            return;
         }
         if(!_core.JobElements.ModIsActive)
         {
            CommonFunctions.DisplayText("Saving new locations only while "
                                        + "the person transport service is activated.",
               3000);
            return;
         }
         using(var locationCapture =
            new LocationCapture(_core.JobElements.ServiceLocationsFullFilePath))
         {
            locationCapture.SaveNewLocation();
         }
         CommonFunctions.DisplayText("Location saved", 2000);
      }
   }
}