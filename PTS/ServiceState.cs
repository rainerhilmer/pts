﻿namespace PTS
{
   public enum ServiceState
   {
      Nothing = 0,
      Start,
      AwaitingJob,
      HeadingForCustomer,
      WithCustomerNoDestination,
      HeadingForDestination,
      CustomerDelivered,
      CustomerDeliveryFailed,
      ShutDownRequested,
      ShutdownApproved,
      BackAtParkingSpot,
      BackAtOfficeDesk,
      HeadingBackToHQ,
      StartOverDueToLostVehicle,
      CustomerEntersVehicle
   }
}